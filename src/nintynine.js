import { LitElement, html } from "lit-element";
import range from "underscore/modules/range.js"
import shuffle from "underscore/modules/shuffle.js";

class Board extends LitElement {
	static get properties() {
		return {
			deck: {
				type: Array
			},
			piles: {
				type: Array
			},
			cardInHand: {
				type: Number,
			},
			nextAutoPile: {
				type: Number,
			},
			autoplay: {
				type: Boolean,
			},
			autoplayGamesPlayed: {
				type: Number,
			},
			wonGame: {
				type: Boolean
			}
		};
	}
	
	constructor() {
		super();
	
		this.wonGame = false;
		this.autoplayGamesPlayed = 0;

		this.newGame();
	}

	newGame() {
		this.deck = shuffle(range(1,99));
		this.piles = [
			{ title: "+", direction: "ascending", cards: [0], valid: false },
			{ title: "+", direction: "ascending", cards: [0], valid: false },
			{ title: "-", direction: "descending", cards: [99], valid: false },
			{ title: "-", direction: "descending", cards: [99], valid: false },
			{ title: "deck", cards: this.deck, isDeck: true}, ];

		console.log('new game');
		console.log(this.deck);		
	}

	async getMoreState() {
		return;
	}

	async selectCard(e, card) {
		this.cardInHand = card;

		for (let pile of this.piles) {
			if(!pile.isDeck) {
				pile.valid = false;
				const cardOnBoard = pile.cards.slice(-1)[0];

				if(pile.direction == "ascending") {
					if(this.cardInHand > cardOnBoard) {
						pile.valid = true;
					}
					if(cardOnBoard-10 == this.cardInHand) {
						pile.valid = true;
					}
				}
				if(pile.direction == "descending") {
					if(this.cardInHand < cardOnBoard) {
						pile.valid = true;
					}
					if(cardOnBoard+10 == this.cardInHand) {
						pile.valid = true;
					}
				}
			}
		}

		this.requestUpdate();
		await Promise.all([this.updateComplete, this.getMoreState()]);
	}

	async playCard(e, pile) {
		this.nextAutoPile = undefined;
		pile.cards.push(this.deck.splice(this.deck.indexOf(this.cardInHand),1)[0]);
		console.log(`card played: ${this.cardInHand}`);
		console.log(this.piles);
		this.cardInHand = undefined;
		for (let pile of this.piles) {
			if('valid' in pile) {
				pile.valid = false;
			}
		}
		//this.stack(this.deck)
		this.requestUpdate();
		await Promise.all([this.updateComplete, this.getMoreState()]);
	}

	async startAutoplay(e) {
		this.autoplay = true;

		this.requestUpdate();
		await Promise.all([this.updateComplete, this.getMoreState()]);
	}

	async loadState(e) {
		console.log("//todo load state")

		this.requestUpdate();
		await Promise.all([this.updateComplete, this.getMoreState()]);
	}

	playScore(card) {
		let scores = [];
		for (const pile of this.piles) {
			let score = -100;
			if(!pile.isDeck) {
				const cardOnBoard = pile.cards.slice(-1)[0];

				if(pile.direction == "ascending") {
					if(card > cardOnBoard) {
						const cardsSkipped = range(cardOnBoard, card);
						score = cardsSkipped.reduce((acc, c) => acc + this.deck.includes(c), 0) * -1;
						if(this.deck.includes(cardOnBoard-10)) {
							score -= 0.1 * (100 - this.deck.indexOf(cardOnBoard-10));
						}
						if(this.deck.includes(card+10)) {
							score -= 0.1 * (100 - this.deck.indexOf(card+10));
						}
					}
					if(cardOnBoard-10 == card) {
						score = 10;
					}
				}
				if(pile.direction == "descending") {
					if(card < cardOnBoard) {
						const cardsSkipped = range(card, cardOnBoard);
						score = cardsSkipped.reduce((acc, c) => acc + this.deck.includes(c), 0) * -1;
						if(this.deck.includes(cardOnBoard+10)) {
							score -= 0.1 * (100 - this.deck.indexOf(cardOnBoard+10));
						}
						if(this.deck.includes(card-10)) {
							score -= 0.1 * (100 - this.deck.indexOf(card-10));
						}
					}
					if(cardOnBoard+10 == card) {
						score = 10;
					}
				}
				
				scores.push(score);
			}
		}

		return scores;
	}

	updated() {
		if(this.autoplay) {
			if(this.cardInHand == undefined) {
				// map the cards to their play scores
				const scoreArrays = this.deck.slice(0, 8).map(card => this.playScore(card));
				const plays = scoreArrays.map((array, index) => {
					const max = Math.max(...array);

					return {
						max: max,
						pile: array.findIndex(card => card == max),
						index: index,
					}
				})
				const bestPlay = plays.find(play => play.max == Math.max(...plays.map(p => p.max)));
				if(bestPlay == undefined || bestPlay.max == -100) {
					this.autoplayGamesPlayed += 1;
					if(this.deck.length) {
						// this.newGame();
						this.autoplay = false;
						return;
					} else {
						this.autoplay = false;
						this.wonGame = true;
					}
				}
				if(bestPlay == undefined) return;
				const cardToSelect = this.deck[bestPlay.index];
				this.nextAutoPile = bestPlay.pile;
				this.selectCard(null, cardToSelect);
			} else if(this.nextAutoPile != undefined) {
				this.playCard(null, this.piles[this.nextAutoPile]);
			}
		}
	}

	render() {
		return html`
			<button @click="${e => this.startAutoplay(e)}">autoplay</button>
			<a href="data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(this.piles))}" download="state.json">save state</a>
			<input type="file" @change="${e => this.loadState(e)}">load state</button>
			${this.wonGame ? html`<div>won in ${this.autoplayGamesPlayed} games</div>` : html`<div></div>`}
			<div style="display: flex; font-family: monospace; text-align: center;">
				${this.piles.map(
					pile => html`
						<ol style="list-style-type: none; flex-direction: column; display: flex;">
							${pile.valid
								? html`
							<button @click="${e => this.playCard(e, pile)} slot="title">${
										pile.title
									} </button>`
								: html`<button disabled slot="title">${pile.title}</button>`}
							${pile.cards && pile.cards.map((card, index) => !pile.isDeck ?
								html`<li>${card}</li>` :
								index < 8 ?
									this.cardInHand != card ?
										html`<button @click="${e => this.selectCard(e, card)}">${card}</button>` :
										html`<button disabled>${card}</button>` :
									html`<div>${card}</div>`
							)}
						</ol>
					`
				)}
			</div>
		`;
	}
}

customElements.define("nintynine-board", Board);
