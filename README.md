# nintynine

https://briaguya0.gitlab.io/99/

based on https://boardgamegeek.com/boardgame/173090/game

it's ugly and the code is messy but the basic 1 player game logic is in there

## Getting started

Clone this repository and install its dependencies:

```bash
git clone https://gitlab.com/briaguya0/99.git
cd 99
npm install
```

The `public/index.html` file contains a `<script src='bundle.js'>` tag, which means we need to create `public/bundle.js`. The `rollup.config.js` file tells Rollup how to create this bundle, starting with `src/main.js` and including all its dependencies.

`npm run build` builds the application to `public/bundle.js`, along with a sourcemap file for debugging.

`npm start` launches a server, using [serve](https://github.com/zeit/serve). 

`npm run watch` will continually rebuild the application as your source files change.

`npm run dev` will run `npm start` and `npm run watch` in parallel.
